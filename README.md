# snowsync-cfg-tst
> В текущий момент является не больше чем тестом (черновиком), вскоре перейдет в **dev** версию - https://gitlab.com/justgitlabaccount/snowsync-cfg-dev

Проект создан для того чтобы добиться воспроизводимости структуры телеграмм вэб [приложения](https://github.com/Latand/MedSyncWebApp/tree/main), на данный момент этого можно осуществить запуском **main.py**  

Идея проекта не ограничивается этим, а ставит своей целью **полную воспроизводимость инфрастуктры вокруг приложения**, т.е. следование принципам **iac** (как я понимаю их сейчас)

Так же **CI/CD** на базе гитлаб - ([фронт](https://gitlab.com/justgitlabaccount/snowsync-front/-/blob/main/.gitlab-ci.yml) | [бэкенд](https://gitlab.com/justgitlabaccount/snowsync-back/-/blob/main/.gitlab-ci.yml))
- фронт - https://gitlab.com/justgitlabaccount/snowsync-front
- бэкенд - https://gitlab.com/justgitlabaccount/snowsync-back  
- "консоль управления" - https://gitlab.com/justgitlabaccount/snowsync-cfg
- приложение - https://t.me/SnowSyncBot
- сайт - https://snowsync.ru/ (возможно тут будет вэб версия приложения)
- прометей - https://prom.snowsync.ru/ (временно удален)

Пока коммиты летят ради отладки и оптимизации, чуть позже все три репы уйдут в архив и будут созданны аналогичные, где уже будет задействован **gitlab flow** и прочие флоу, добавятся другие сервисы, появится нормальная дока (в нее уйдет все что тут написанно, только все будет более структурированно)...

# What?

При запуске **main.py** произойдет создание и настройка сервера (+ запуск плейбука) основываясь на [тарифах](https://www.netangels.ru/cloud/#tariffs) | в **main.py** выведена вся промежуточная логика | по большой части запуск **main.py** уже делает что написанно выше, остались мелкие аспекты в автоматизации, которые уже разумней реализовывать в **snowsync-cfg-dev**

### Основные функции
- Создание ВМ с необходимымы параметрами + выбор плейбука (по названию) который будет запущен
- Другие функции по работе с ВМ (смена тарифа/переустановка ОС + системные)
- Управление DNS зонами и записями (создание/обновление)
- Управление сетью (добавить в локалку)
- Управление SSH (добавить паблик ключ к ВМ)
- В какой среде развернуть tst/dev/stg/prod | для дальнейших нужд инфрастуктуры
- Обертки на питоне для работы с Ansible, k8s, вспомогательные по работе с ВМ 
- что-то еще...

### Плейбуки
- **app-compose-setup.yml** - плейбук для настройки виртуалки и поднятия на ней приложения в компоузе
- **glab-runner-setup.yml** - плейбук для настройки гитлаб раннера
- **prometheus-setup.yml** - плейбук для настройки прометея + обратный прокси...
- **k8s-setup.yml** - плейбук для поднятия кластера кубика пока с одним мастером и любым кол-вом нод | нужно реализовать остальные фишки и добавить возможность деплоя с multi-master
- **consul_nomad_setup.yml** - [3 сервера / 2 клиента](https://github.com/justgithubaccount/ff-consul-nomad) | оптимизировать по бэст практис, чтобы было рабочее прод решение | пришло тестовое часть функционала ушло в проект на гитхабе и там немножко обновилось
- **basic-security-setup.yml** - что-то будет про самуб базовую безопасность...

## Folders

- **api** - обертки для питона | [api](https://api.netangels.ru/) хостинга для создания виртуалок, днсов и прочего  
- **ansible** - конфиги (роли, плейбуки) ансибла, основные настройки  
- **docker** - докер-компоуз конфиг для инфры под приложение | и другие
- **terraform** - на будущие, пока нет особого смысла его использовать для создания 3.5 виртуалок, позже будет использован для кубика на digital ocean и прочих нужд на других облаках  
- **orch/k8s** - comming soon
- **orch/helm** - comming soon  
- **scripts** - скрипты с которых все началось (особо не нужны)  
- **utils** - обертки для питона для тулзов

# Tasks
> Так или иначе уже будет переделанно в dev-версии - https://gitlab.com/justgitlabaccount/snowsync-cfg-dev
### Nuances
- **main.py** необходимо выполнять в ./ansible ~~(нужно переделать пути)~~  
  `jenya@debian ~/gitlab/snowsync-cfg/ansible (main*?) $ /bin/python3 ../main.py`  
  (запуск для того чтобы функция работала корректно)

### GitLab API + glab
- задейстовать api гитлаба (для обновления/добавления переменных и всего остального) + [glab](https://gitlab.com/gitlab-org/cli)
    - ~~добавить ролей для гитлаб ранера~~ (~~сделать плейбук~~), апи гитлаба тут пригодится 
    - покапаца в DinD ругается в [начале логов](https://gitlab.com/justgitlabaccount/snowsync-front/-/jobs/6772536250), но продолжает работу
    - ставить галочку "Run untagged jobs" после создания раннера через **main.py**
    - ~~инициировать запрос на создания ранера~~

### Python 
- ~~добавить оберток для api netangels для днс [зон](https://api.netangels.ru/modules/gateway_api.api.dns.zones/)~~ и ~~[записей](https://api.netangels.ru/modules/gateway_api.api.dns.records/)~~ | ~~добавить смену тарифа на nmve/sas~~ | ~~на переустановку ос~~
- добавить доп логику формирования и проверки в инвентори dev.ini / prod.ini | если есть уже такое имя сервера, например glab-runner, то если при запускать **main.py** с таким же именем, то он добавлял что-то типо glab-runner-###
- ~~перевести проект на poetry~~ | ~~переформировать структуру снова~~ 😓
- использовать jinja2 в питоне (формировать конфиги) и ~~environs, betterlogging~~...

### Ansible
- Прочекать на идемпотентность 
- Линтер ансибла говорит что все очень плохо 😀
- Где-то там мелькали либы питона для ансибла и прочие апишки | заресерчить вообще все что можно использовать для автоматизации в контексте питона/go/javascript | мб возможно отказаться от питона, а сделать все на nodejs, ну или go

### Terraform
- ~~протестить конфиг терраформа (env-work) где нибудь на яндексе~~ | 6 подсетей в каждой по 1-2 вм | в подсети для кубика 3 | pfsense как nat | дорого тестить
- запустить кубик ~~в каком нибудь облаке~~, либо на основе виртуалок на том же netangels
    - ~~на digital ocean 12 баксов за одну самую дешевую ноду - 2 GB total RAM / 1 vCPU / 50 GB storage на SSD дисках (мастер в комплекте)~~
    - на netangels будет дешевле всего (~1к в месяц за 3 сервера)
    - ~~на яндексе дороже (1к рублей за самый медленный сервер...)~~
- Посмотреть что предлагает обертка terragrunt | хотя терраформ в маленьких проектах все видется лишним | больше инструмент для тех у кого есть деньги и когда нужно поддерживать большие инфрастуктуры, ну и клауд-нейтив на деле, а не в голове 

### Infrastructure
- добавить ~~прометеус~~ + графана, алертменеджер...
- добавить можно много всего, стек elk есть, но больше нравится grafana-релейтед prometheus + loki + grafana

### Security
- по сути все начинается с тех кто дает деньги бизнесу и **на полном недоверии к друг другу**, а в это не все могут, по большой части devsecops и другая sec-панацея прост накидывает абстракций поверх всего этого, забывая о канонах [???]
- вообще в паблике висят готовые пресеты по гпошкам (GPO что-то типо ансибла встроенного по умолчанию в Active Directory) от министерства обороны сша до кибербезопасности франции, скорей всего такое есть и для всего остального в контексте опен-сурс | заресерчить
- добавить ваулт | ~~задействовать ансибле ваулт~~
- добавить авторизацию для прометуса, пока пусть висит в паблике 🙈 | а надо ли? | спрятать в локалку
- добавить функций в **ssh_key_mng.py** (продумать логику базовой безопасности)
- freeipa было бы очень не плохо вообще не использовать локальные учетки в linux подобных средах, как selinux и остальные, freeipa вроде дает таку возможность | централизованная настройка | как freeipa может в откаусточивость? (multi-master?) | по сути внедрея ее она ставится одним из самых важных элементов системы
### Prometheus 
- Использовать `pip install prometheus-client` | [client_python](https://github.com/prometheus/client_python)
- Пополнять инфу об агентах автоматически

## Errors
- Раннер уходит в [ошибку](https://www.google.com/search?q=ERROR%3A+error+during+connect%3A+Get+%22http%3A%2F%2Fdocker%3A2375%2Fv1.24%2Finfo%22%3A+dial+tcp%3A+lookup+docker+on+91.201.54.5%3A53%3A+no+such+host) ([лог гитлаба](https://gitlab.com/justgitlabaccount/snowsync-front/-/jobs/6779308252)) после создания, исправляется добавлением строки `"/var/run/docker.sock:/var/run/docker.sock"` в volumes в config.toml | нужно понять природу ошибки | добавления тома позволило общаться докер демону с хостом | наверное такое происходит из-за dind
- Let's Encrypt забанил за [множественный выпуск одного и того сертификата](https://letsencrypt.org/docs/duplicate-certificate-limit/)
  
## Edu 
Информация так или иначе которая больше всего помогла в освоение "нового"
### Main
- [thoughts-come-from-here.jpg](https://www.reddit.com/r/devops/comments/14hl4ha/comment/jpcampc/)
- [evgeniy-kharchenko/The-Way-of-DevOps: Проект посвящённый развитию в DevOps направлении](https://github.com/evgeniy-kharchenko/The-Way-of-DevOps)
- [The Modern JavaScript Tutorial](https://javascript.info/)
- [Основы GNU/Linux и подготовка к RHCSA](https://basis.gnulinux.pro/ru/latest/course.html)
- [Linux Command Line Books by William Shotts](https://linuxcommand.org/tlcl.php)
- [MLOps и production подход к ML исследованиям 2.0 — Open Data Science](https://ods.ai/tracks/ml-in-production-spring-23)
- [MedSync WebApp Documentation - MedSync Telegram Mini App](https://docs.medsync.botfather.dev/)
### Other
- [Advanced End-to-End CICD Pipeline for a Java Web Application: A Step-by-Step Guide](https://mandeepsingh10.hashnode.dev/advanced-end-to-end-cicd-pipeline-for-a-java-web-application-a-step-by-step-guide)
- [The best DevOps project for a beginner | Logan Marchione](https://loganmarchione.com/2022/10/the-best-devops-project-for-a-beginner/)
- [GitLab Documentation](https://docs.gitlab.com/)
- [Tutorials | DigitalOcean](https://www.digitalocean.com/community/tutorials)