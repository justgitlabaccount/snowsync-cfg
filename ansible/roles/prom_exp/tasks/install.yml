---
- name: Check if Node Exporter binary is already in /usr/bin
  ansible.builtin.stat:
    path: /usr/bin/node_exporter
  register: node_exporter_bin

- name: Ensure user node_exporter exists
  ansible.builtin.user:
    name: node_exporter
    shell: /bin/false
    system: yes
    createhome: no
  when: not node_exporter_bin.stat.exists  

- name: Download Node Exporter
  ansible.builtin.get_url:
    url: "https://github.com/prometheus/node_exporter/releases/download/v{{ node_exporter_version }}/node_exporter-{{ node_exporter_version }}.linux-amd64.tar.gz"
    dest: "/tmp/node_exporter-{{ node_exporter_version }}.linux-amd64.tar.gz"
    mode: 0755
  when: not node_exporter_bin.stat.exists

- name: Extract Node Exporter
  ansible.builtin.unarchive:
    src: "/tmp/node_exporter-{{ node_exporter_version }}.linux-amd64.tar.gz"
    dest: "/tmp/"
    creates: "/tmp/node_exporter-{{ node_exporter_version }}.linux-amd64"
    remote_src: true
  when: not node_exporter_bin.stat.exists

- name: Move Node Exporter binary to /usr/bin/
  ansible.builtin.command: mv /tmp/node_exporter-{{ node_exporter_version }}.linux-amd64/node_exporter /usr/bin/
  when: not node_exporter_bin.stat.exists

- name: Set ownership of Node Exporter binary
  ansible.builtin.file:
    path: /usr/bin/node_exporter
    owner: node_exporter
    group: node_exporter
    mode: '0755'
  when: not node_exporter_bin.stat.exists

- name: Cleanup downloaded and extracted files
  ansible.builtin.file:
    path: "/tmp/node_exporter-{{ node_exporter_version }}*"
    state: absent
  when: not node_exporter_bin.stat.exists

- name: Configure systemd service for Node Exporter
  ansible.builtin.template:
    src: node_exporter.service.j2
    dest: /etc/systemd/system/node_exporter.service
  notify:
    - restart node_exporter
