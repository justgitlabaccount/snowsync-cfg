import requests
import json

def get_zone_id_by_name(api_token, zone_name):
    """Получить ID DNS-зоны по имени зоны."""
    url = "https://api-ms.netangels.ru/api/v1/dns/zones/"

    headers = {'Authorization': f'Bearer {api_token}'}

    response = requests.get(url, headers=headers)

    if response.status_code == 200:
        zones = response.json()
        for zone in zones.get('entities', []):
            if zone['name'] == zone_name:
                return zone['id']
        print(f"No zone found with name: {zone_name}")
        return None
    else:
        raise Exception(f"Failed to fetch DNS zones: {response.status_code}, {response.text}")

def get_dns_records(api_token, zone_id):
    """ Получить все DNS-записи для данной зоны. """

    url = f"https://api-ms.netangels.ru/api/v1/dns/zones/{zone_id}/records/"

    headers = {'Authorization': f'Bearer {api_token}'}

    response = requests.get(url, headers=headers)

    if response.status_code == 200:
        return response.json()['entities']
    else:
        raise Exception(f"Failed to fetch DNS records: {response.text}")

def find_record(records, record_name, record_type):
    """ Поиск конкретной записи по имени и типу. """
    for record in records:
        if record['name'] == record_name and record['type'] == record_type:
            return record
    return None

def create_dns_record(api_token, zone_name, record_name, record_type, ip_address):
    """ Создать новую DNS-запись. """
    zone_id = get_zone_id_by_name(api_token, zone_name)
    if not zone_id:
        raise Exception(f"Zone {zone_name} not found, cannot proceed with DNS record creation.")

    url = "https://api-ms.netangels.ru/api/v1/dns/records/"
    headers = {
        'Authorization': f'Bearer {api_token}',
        'Content-Type': 'application/json'
    }
    
    data = {
        'name': record_name,
        'type': record_type,
        'ip': ip_address
    }

    response = requests.post(url, headers=headers, json=data)
    if response.status_code in [200, 201]:
        print("DNS record created successfully.")
        return response.json()
    else:
        raise Exception(f"Error {response.status_code} during record creation: {response.text}")

def update_dns_record(api_token, record_id, record_name, ip_address):
    """ Обновить существующую DNS-запись. """
    url = f"https://api-ms.netangels.ru/api/v1/dns/records/{record_id}/"

    headers = {
        'Authorization': f'Bearer {api_token}',
        'Content-Type': 'application/json'
    }

    data = {
        'name': record_name,
        'ip': ip_address
    }

    print(f"update_dns_record - {data}, record_id - {record_id}")

    response = requests.post(url, headers=headers, json=data)

    if response.status_code in [200, 201]:
        print("DNS record updated successfully.")
        return response.json()
    else:
        raise Exception(f"Error {response.status_code} during record update: {response.text}")

def config_dns_record(api_token, zone_name, record_name, record_type, ip_address):
    """ Создать или обновить DNS-запись в зависимости от её наличия. """
    zone_id = get_zone_id_by_name(api_token, zone_name)
    if not zone_id:
        raise Exception(f"Zone {zone_name} not found, cannot proceed with DNS record modification.")

    records = get_dns_records(api_token, zone_id)
    print(f"get_dns_records - {records}")
    record = find_record(records, record_name, record_type)
    print(f"find_record - {record}")

    if record:
        return update_dns_record(api_token, record['id'], record_name, ip_address)
    else:
        return create_dns_record(api_token, zone_name, record_name, record_type, ip_address)
