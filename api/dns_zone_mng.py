# https://api.netangels.ru/modules/gateway_api.api.dns.zones/#dns-_1

import requests
import json

def create_dns_zone(api_token, name):
    """Создает DNS-зону, если она не существует."""

    if check_zone_exists(api_token, name):
        print(f"DNS zone '{name}' already exists")
        return None

    url_dns_zone = "https://api-ms.netangels.ru/api/v1/dns/zones/"

    headers = {
        'Authorization': f'Bearer {api_token}',
        'Content-Type': 'application/json'
    }

    data = {
        'name': name
    }

    json_data = json.dumps(data)

    response = requests.post(url_dns_zone, headers=headers, data=json_data)

    if response.status_code == 201:
        return response.json()
    else:
        raise Exception(f"Error: {response.status_code}, {response.text}")
    
def check_zone_exists(api_token, name):
    """Проверяет, существует ли DNS-зона с указанным именем."""

    url = "https://api-ms.netangels.ru/api/v1/dns/zones/"

    headers = {'Authorization': f'Bearer {api_token}'}

    response = requests.get(url, headers=headers)

    if response.status_code == 200:
        zones = response.json()
        for zone in zones.get('entities', []):
            if zone['name'] == name:
                return True
    return False