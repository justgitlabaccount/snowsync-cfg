import os

from dotenv import load_dotenv

load_dotenv() 

API_KEY = os.getenv('API_KEY', "default_api_key_if_none")
SSH_KEY_VALUE = os.getenv('SSH_KEY_VALUE', "default_ssh_key_if_none")
SSH_KEY_ID = int(os.getenv('SSH_KEY_ID', "default_ssh_id_if_none"))