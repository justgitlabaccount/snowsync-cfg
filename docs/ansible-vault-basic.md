https://docs.ansible.com/ansible/2.9/user_guide/vault.html

Создать хранилку секретов  
`EDITOR=nano ansible-vault create secrets.yml`

Зайти и изменить  
`EDITOR=nano ansible-vault edit secrets.yml`

Задать по умолчанию редактор  
`export EDITOR=nano`

**Текущие значения в хранилке**  
`glab_runner_token: "glrt-###################"`

Запуск плейбука с паролем от хранилки  
``ansible-playbook -l glab-runner -i ./inventory/dev.ini ./playbooks/glab-runner-setup.yml --check --ask-vault-pass``

Запуск плейбука с паролем сохраненным в файле  
`ansible-playbook -l glab-runner -i ./inventory/dev.ini ./playbooks/glab-runner-setup.yml --check --vault-password-file vault_password.txt`