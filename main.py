from auth import get_token

from api.vm_mng import reinstall_os, change_tariff
from api.ssh_key_mng import upload_ssh_key_to_vm
from api.dns_zone_mng import create_dns_zone
from api.dns_records_mng import config_dns_record

from utils.ansible_mng import run_playbook, update_inventory
from utils.vm_setup import setup_vm, create_vm
from utils.k8s_mng import create_cluster

from config import API_KEY

if __name__ == '__main__':
    # Идея в том чтобы задеплойть приложение и всю инфрастуктуры для него запуском "main.py"
    # Есть условный клиент которому нужно приложение (тг-вэб-апп + деплой куда-то там (не важно))
    # ...
    # В случае этого кода
    # - репы под фронт и бэк на гитлабе для того чтобы в моменте запуска брать код с них
    #   и с какой-то базовой настройкой ci, для дальнейшей поддержки
    # - создание раннера(ов) под эти репы
    # - подъем приложения в докер-компоуз/кубер (в средах tst/dev/stg/prod для приложения)
    # - опционально среды wrk/srv для компов/серверов...
    # - прометей для мониторинга (надо продумать как это все автомазировать)
    # - ...
    # Ну и дальнеиший шаг привязать "main.py" к какой-то вэбке/коммиту 
    # чтобы бы был интефейс по созданию и управлению созданными инстансами (App Cloud)
    # ...
    # В какой-то степени отрожает наследие идеи создания подобной структуры вокруг AD (Active Directory)
    # Где по сути ad нужно для выстраивания иб + отказоустойчивость из коробки (multi-master)
    # + допом интеграция со всем продуктами майрософта = экосистема = приложение
    # ...

    ########################
    ### Настройки деплоя ###
    ########################

    # Ключ доступа к API
    api_token = get_token(API_KEY)

    # Эталонный образ используемый в инфраструктуре
    golden_img = 'img_debian-bookworm'
    # Тип среды
    env_type = 'tst' # tst/dev/stg/prd
    # Тарифы по умолчанию
    tariff_sas = 'start_2' # Все тарифы start_# (1-4) имееют в своей основе SAS диски
    tariff_nvme = 'tiny' # Все остальные тарифы (tiny, small, medium, large и другие) имееют NVMe диски
    # Имя проекта
    prj_name = 'syncjob' # glab_proj_pre_name (отношение к плейбуку app-compose-setup)

    ### Где-то тут конфиг для настройки Vault

    # Создать репозиторий на гитлабе (предполагаются репы с именами snowsync-front и snowsync-back)
    # Какая-то апишка для создания | лучше наверно будет создавать группу и уже в ней проекты
    # https://docs.gitlab.com/ee/api/projects.html#create-project
    
    # Cоздать зону DNS под приложение (паблик домен) | возможно еще приватный домен in.snowsync.ru (FreeIPA)
    dns_zone = create_dns_zone(api_token, f'{prj_name}.ru')

    ### Где-то тут конфиг для настройки OPNsense
    ### Где-то тут конфиг для настройки FreeIPA
    ### Где-то тут конфиг для настройки Loki

    ###################################
    ### Cоздание Prometheus сервера ###
    ###################################

    # Формирование имени основываясь на типе среды | tst-prometheus
    srv_prom_name = f'{env_type}-prometheus'
    # Создание ВМ 
    srv_prom_vm = setup_vm(api_token, srv_prom_name, tariff_nvme, 10, golden_img, False, env_type)
    # Создание поддомена | бывает не успевает обновится до момента запуска таска в плейбуке по получению сертификата 
    srv_prom_ip = srv_prom_vm['main_ip']
    srv_prom_dns = config_dns_record(api_token, f'{prj_name}.ru', f'prom.{prj_name}.ru', 'A', srv_prom_ip)

    if srv_prom_vm:
        run_playbook(env_type, 'prometheus-setup')
        change_tariff(api_token, srv_prom_name, tariff_sas)
        upload_ssh_key_to_vm(api_token, srv_prom_name)

    ####################################
    ### Cоздание App Compose сервера ###
    ####################################
    # srv_app = 'app-compose' # ВМ на которой запускается приложение
    # setup_vm(api_token, srv_app, tariff_nvme, 10, golden_img, True, env_type, 'app-compose-setup')

    ######################################
    ### Cоздание GitLab Runner сервера ###
    ######################################
    # srv_runner = 'glab-runner' 
    # setup_vm(api_token, srv_runner, tariff_nvme, 10, golden_img, False, env_type, 'glab-runner-setup')

    #################################
    ### Создание App k8s кластера ###
    #################################

    # master_tariff = 'small' # 2 ядра, 2 гига, nmve
    # nodes_tariff = 'tiny' # 1 ядро, 2 гига, nvme
    # num_nodes = 2

    # create_cluster(api_token, num_nodes, master_tariff, nodes_tariff, 10, golden_img, False)

    ############################################
    ### Создание App Consul + Nomad кластера ###
    ############################################

    # # Создание кластера из 3 серверов и 2 клиентов в среде tst
    # num_srv_node = 3
    # num_cli_node = 2
    # create_cluster(api_token, num_srv_node, num_cli_node, tariff_nvme, golden_img, env_type)