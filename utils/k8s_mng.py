from api.vm_mng import create_vm, is_vm_ready
from api.net_mng import add_to_local_net
from utils.ansible_mng import run_playbook, generate_k8s_inventory

def create_cluster(api_token, num_nodes, master_tariff, node_tariff, disk_size, golden_img, is_backup_enabled):
    """
    Сreation Kubernetes cluster

    :param api_token: API token from NetAngels
    :param num_node: Number of nodes
    :param master_tariff: Tariff for master node
    :param node_tariff: Tariff for nodes
    :param disk_size: Disk size for master node and nodes
    :param golden_img: Golden image OS
    :param is_backup_enabled: Backup?

    :return: API response as JSON or error message
    """
    master_name = 'master'

    # Создание мастера 
    k8s_master = create_vm(api_token, master_name, master_tariff, disk_size, golden_img, is_backup_enabled)

    vm_id = k8s_master['id']

    if is_vm_ready(api_token, vm_id):
        add_to_local_net(api_token, master_name)

    # Создание нод
    nodes = []
    for i in range(1, num_nodes + 1):
        node_name = f'k8s_node_{i:02d}'
        node = create_vm(api_token, node_name, node_tariff, disk_size, golden_img, is_backup_enabled)

        vm_id = node['id']

        if is_vm_ready(api_token, vm_id):
            add_to_local_net(api_token, node_name)
            
        nodes.append(node)

    # Формирование инвентори файла
    if k8s_master and all(nodes):
        master_ip = k8s_master['main_ip']
        node_ips = [node['main_ip'] for node in nodes]

        generate_k8s_inventory(master_ip, node_ips)
        
        # Запуск плейбука после создания инвентори файла
        run_playbook('prd', 'k8s-setup')
        print("Cluster created.")
    else:
        print("Failed to create cluster.")